Rails.application.routes.draw do
  devise_for :users

  get 'pages/dashboard'
  get 'pages/inventory'
  get 'pages/contacts'

  get 'hello_world', to: 'hello_world#index'

  root 'pages#index'
end
